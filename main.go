package main

import (
	"bufio"
	"fmt"
	"io/ioutil"

	//"io/ioutil"
	"os"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func isWord(fName string, w string) bool {
	f, err := os.Open(fName)
	check(err)
	defer f.Close()

	scanner := bufio.NewScanner(f)
	scanner.Split(bufio.ScanWords)

	//var words []string
	for scanner.Scan() {
		if scanner.Text() == w {
			return true
		}
	}
	return false
}

func main() {
	filesWithWord := []string{}

	scanner1 := bufio.NewScanner(os.Stdin)
	scanner2 := bufio.NewScanner(os.Stdin)

	fmt.Println("Enter a directory path:")
	scanner1.Scan()
	fmt.Println("Enter the word to search for:")
	scanner2.Scan()
	dir := scanner1.Text()
	word := scanner2.Text()

	files, err := ioutil.ReadDir(dir)
	check(err)
	for _, file := range files {
		if file.Mode().IsDir() == false {
			if isWord(file.Name(), word) {
				filesWithWord = append(filesWithWord, file.Name())
			}
		}
		//fmt.Printf("name:%v\ttype: %s\n", file.Name(), modeString(file.Mode()))
	}
	if len(filesWithWord) == 0 {
		fmt.Println("No files with the word provided.")
	} else {
		fmt.Println("Files with the word", word+":")
		for _, v := range filesWithWord {
			fmt.Println("-->", v)
		}
	}
}
